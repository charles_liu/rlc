define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/about.html"
],
    function ($, _, Backbone, Handlebars, template) {

        var aboutView = Backbone.View.extend({

            events: {
            },

            initialize: function (options) {
            },

            template: Handlebars.compile(template),

            render: function () {
                $(this.el).html(this.template({}));
                return this;
            }
        });

        return aboutView;

    });