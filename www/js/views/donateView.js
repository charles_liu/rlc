define(["jquery", "underscore", "backbone", "handlebars", "constants",
    "text!templates/donate.html"
],
    function ($, _, Backbone, Handlebars, C, template) {

        var donateView = Backbone.View.extend({

            events: {
                "click #submit": "submitClick"
            },

            initialize: function (options) {
            },

            template: Handlebars.compile(template),

            render: function () {
                var titleText = "We will be contacting you through your Gmail account. If you wish for us to contact " +
                    "you with another email, please contact us directly at " + constants["contactEmail"];
                $(this.el).html(this.template({titleText: titleText}));
                return this;
            },

            submitClick: function(event) {
                var d = new Date();
                var start = {
                    "dateTime": d.toISOString()
                };
                d.setMinutes(d.getMinutes() + 30);
                var end = {
                    "dateTime": d.toISOString()
                };
                var insertHash = {
                    "summary": "New Donation Request",
                    "description": $('#name').val() + ": " + $('#description').val(),
                    "location": $('#location').val(),
                    "start": start,
                    "end": end,
                    "attendees": [{"email": constants["calendarEmail"]}]
                };
                createEvent(this.model.getCalendarID(), this.model.getAuthToken(), insertHash, 'The team has been notified. We will be contacting you shortly!', 'Could not create an event. Please try again, or you can email us directly at ' + constants["contactEmail"], true, false);
            }
        });

        return donateView;

    });