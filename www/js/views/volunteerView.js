define(["jquery", "underscore", "backbone", "handlebars", "constants",
    "text!templates/eventList.html",
    "views/eventView"
],
    function ($, _, Backbone, Handlebars, C, template, eventView) {

        var volunteerView = Backbone.View.extend({
            title: "Volunteer Opportunities",
            events: {
                "click .eventButton": "volunteerEvent"
            },

            initialize: function (options) {
                this.model.myEvents.fetch();
                this.collection.fetch({reset: true});
                this.listenTo(this.collection, "reset", this.populateEvents);
            },

            template: Handlebars.compile(template),

            render: function () {
                $(this.el).html(this.template({title: this.title, showRefresh: false}));
                return this;
            },

            populateEvents: function() {
                if (this.$el != null) {
                    if (this.collection.length > 0) {
                        this.$('#eventsList').html("");
                        this.collection.first(this.collection.count).forEach(
                            this.addEvent,
                            this
                        );
                    }
                    else {
                        this.$('#eventsList').html("We are currently in the process of gathering donors. Please check back soon!");
                    }
                }
                else {
                    //null hack
                }
                refreshIScroll();
            },

            addEvent: function(event) {
                var eView = new eventView({model: event, attributes: {buttonText: "Add", myEvent: false}});
                this.$('#eventsList').append($(eView.render().el).html());
            },

            volunteerEvent: function(evt) {
                var event = this.collection.get(evt.target.id).attributes;
                var calId = this.model.getCalendarID();
                var accessToken = this.model.getAuthToken();
                var errorText = 'Could not add event. Please contact us at ' + constants["contactEmail"];
                var freeBusyOptions = {
                    type: "GET",
                    url: constants["rootUrl"] + '/' + calId + '/events?timeMax=' + event.end.dateTime + '&timeMin=' + event.start.dateTime,
                    success: function(data) {
                        var importData = {
                            "end": event.end,
                            "start": event.start,
                            "attendees": [{"email": constants["calendarEmail"]}],
                            "summary": event.summary,
                            "description": event.description,
                            "location": event.location,
                            "id": rlcIdConvert(evt.target.id)
                        };
                        if (event["recurrence"]) {
                            importData["recurrence"] = event["recurrence"];
                        }
                        if (data.items.length) {
                            var conflict = false;
                            for (var i = 0 ; i < data.items.length ; i++) {
                                if (data.items[i].attendees) {
                                    for (var j = 0 ; j < data.items[i].attendees.length ; j++) {
                                        if (data.items[i].attendees[j].email.toUpperCase() == constants["calendarEmail"].toUpperCase() && data.items[i].attendees[j].responseStatus == "accepted") {
                                            conflict = true;
                                        }
                                    }
                                }
                                if (data.items[i].id == importData.id) {
                                    conflict = true;
                                }
                            }
                            if (conflict)
                                navigator.notification.alert("You have a conflicting event. Please look at your My Events page.", function() {}, "Conflicting Time");
                            else
                                createEvent(calId, accessToken, importData, 'Thanks for volunteering. We will contact you shortly on your availability!', errorText, false, false)
                        }
                        else {
                            createEvent(calId, accessToken, importData, 'Thanks for volunteering. We will contact you shortly on your availability!', errorText, false, false)
                        }
                    },
                    error: function() {
                        navigator.notification.alert(errorText, function() {}, "Error");
                    },
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + accessToken);
                        xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
                    }
                };
                Backbone.ajax(freeBusyOptions);
            },

            loadMore: function(evt) {
                var oldCount = this.collection.count;
                if (oldCount < this.collection.length) {
                    this.collection.incrementCount();
                    var len = this.collection.count > this.collection.length ? this.collection.length : this.collection.count;
                    for (var i = oldCount ; i < len ; i++) {
                        this.addEvent(this.collection.at(i));
                    }
                    refreshIScroll();
                }
            }
        });

        return volunteerView;

    });