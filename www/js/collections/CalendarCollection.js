//collection of profile models. set urlpath, userid, logintoken prior to fetching
define(["jquery", "underscore", "backbone", "constants",
    "models/CalendarModel"
],
    function($, _, Backbone, C,
             CalendarModel) {

        var CalendarCollection = Backbone.Collection.extend({
            model: CalendarModel,
            count: 5,
            resultCount: 0,
            authToken: "",
            hasMore: true,

            urlRoot: constants["rootUrl"],

            url: function() {
                return this.urlRoot.replace("/calendars",
                    "/users/me/calendarList?maxResults=" + this.count +"&fields=items(accessRole%2Cdescription%2Cid%2Csummary)");
            },

            setAuthToken: function(token) {
                this.authToken = token;
            },

            sync: function(method, model, options){
                options.timeout = 10000;
                options.type = "GET";
                options.headers = {'Authorization': 'Bearer ' + this.authToken};
                return Backbone.sync(method, model, options);
            },
            parse: function(response) {
                if (response["items"]) {
                    var writeCals = [];
                    for (var i = 0 ; i < response["items"].length ; ++i) {
                        if (response["items"][i].accessRole != "reader")
                            writeCals.push(response["items"][i]);
                    }
                    if (this.resultCount == writeCals.length)
                        this.hasMore = false;
                    this.resultCount = writeCals.length;
                    return writeCals;
                }
                return [];
            },

            incrementCount: function() {
                this.count += 5;
                this.fetch({reset: true});
            }
        });

        return CalendarCollection;
    });