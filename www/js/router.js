define(["jquery", "underscore", "backbone", "constants",
    "views/StructureView",
    "views/homeView",
    "views/aboutView",
    "views/donateView",
    "views/volunteerView",
    "views/loginView",
    "views/setCalView",
    "views/myEventsView",
    "models/AuthModel",
    "collections/EventCollection",
    "models/EventModel",
    "collections/CalendarCollection"
],
    function ($, _, Backbone, C, StructureView, homeView, aboutView, donateView, volunteerView, loginView,
              setCalView, myEventsView, AuthModel, EventCollection, EventModel, CalendarCollection) {

        var AppRouter = Backbone.Router.extend({

            authModel: new AuthModel(),

            routes: {
                "": "showStructure",
                "home": "homeView",
                "about": "aboutView",
                "donate": "donateView",
                "volunteer": "volunteerView",
                "myEvents": "myEventsView"
            },

            initialize: function (options) {
                this.currentView = undefined;
            },

            homeView: function () {
                var page = new homeView({
                });
                // show the view
                this.changePage(page);
            },

            aboutView: function () {
                // create the view and show it
                var page = new aboutView({});
                this.changePage(page);
            },

            donateView: function () {
                // create the view and show it
                if (this.authModel.hasAuthToken()) {
                    if (this.authModel.hasCalendarID()) {
                        var page = new donateView({model: this.authModel});
                        this.changePage(page);
                    }
                    else {
                        var calCollection = new CalendarCollection();
                        calCollection.setAuthToken(this.authModel.getAuthToken());
                        var page = new setCalView({collection: calCollection, model: this.authModel});
                        this.changePage(page);
                    }
                }
                else {
                    var page = new loginView({model: this.authModel});
                    this.changePage(page);
                }
            },

            myEventsView: function() {
                if (this.authModel.hasAuthToken()) {
                    if (this.authModel.hasCalendarID()) {
                        this.authModel.myEvents.resetCount();
                        var page = new myEventsView({collection: this.authModel.myEvents, model: this.authModel});
                        this.changePage(page);
                    }
                    else {
                        var calCollection = new CalendarCollection();
                        calCollection.setAuthToken(this.authModel.getAuthToken());
                        var page = new setCalView({collection: calCollection, model: this.authModel});
                        this.changePage(page);
                    }
                }
                else {
                    var page = new loginView({model: this.authModel});
                    this.changePage(page);
                }
            },

            volunteerView: function () {
                // create the view and show it
                if (this.authModel.hasAuthToken()) {
                    if (this.authModel.hasCalendarID()) {
                        var eCollection = new EventCollection();
                        eCollection.configRequest({
                            "request": "GET",
                            "authToken": this.authModel.getAuthToken(),
                            "calendarID": constants["RLCVerified"]
                        });
                        var page = new volunteerView({collection: eCollection, model: this.authModel});
                        this.changePage(page);
                    }
                    else {
                        var calCollection = new CalendarCollection();
                        calCollection.setAuthToken(this.authModel.getAuthToken());
                        var page = new setCalView({collection: calCollection, model: this.authModel});
                        this.changePage(page);
                    }
                }
                else {
                    var page = new loginView({model: this.authModel});
                    this.changePage(page);
                }
            },

            // load the structure view
            showStructure: function () {
                if (!this.structureView) {
                    this.structureView = new StructureView({});
                    // put the el element of the structure view into the DOM
                    document.getElementsByTagName('body')[0].appendChild(this.structureView.render().el);
                    document.addEventListener("resume", _.bind(this.authModel.checkAuthToken, this.authModel), false);
                    this.authModel.checkAuthToken();
                }
                this.homeView();
            }

        });

        return AppRouter;

    });