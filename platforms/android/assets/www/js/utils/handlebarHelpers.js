define(['handlebars'], function(Handlebars) {
    //register any handlebar helpers
    Handlebars.registerHelper('timeString', function(time, recurrence){
        var d = new Date();
        d.setISO8601(time.dateTime);
        var timeArr = d.toLocaleTimeString().split(':');
        var timeStr = "";
        if (timeArr[0] >= 12) {
            timeStr = "PM";
        }
        else {
            timeStr = "AM";
        }
        if (timeArr[0] > 12) {
            timeStr = (timeArr[0] - 12) + ":" + timeArr[1] + " " + timeStr;
        }
        else {
            timeStr = timeArr[0] + ":" + timeArr[1] + " " + timeStr;
        }
        if (recurrence) {
            var recArr = recurrence[0].split(';');
            var str = "";
            for (var i = 0 ; i < recArr.length ; i++) {
                if (recArr[i].indexOf('FREQ') >= 0) {
                    str += (recArr[i].split('='))[1];
                }
                else if (recArr[i].indexOf('BYDAY') >= 0) {
                    str += " ON " + (recArr[i].split('='))[1];
                }
            }
            return str + ' AT ' + timeStr;
        }
        else {
            return d.toLocaleDateString() + " " + timeStr;
        }
    });

    Handlebars.registerHelper('eventStatus', function(event){
        return getRLCResponse(event);
    });

    Handlebars.registerHelper('mapLaunch', function(query){
        var platform = device.platform;
        query = encodeURI(query);
        if (platform.toUpperCase().indexOf("ANDROID") >= 0) {
            return "geo:0,0?q=" + query;
        }
        else if (platform.toUpperCase().indexOf(("IOS")) >= 0) {
            return "maps:q=" + query;
        }
        return "http://maps.google.com/?q=" + query;
    });

    Handlebars.registerHelper('getResponse', function(text){
        return constants[text];
    });

    Handlebars.registerHelper('formatTitle', function(title){
        var frag = Backbone.history.fragment;
        var prev = '<label class="block-label">';
        var post = '</label>';
        if (frag == "myEvents") {
            var htmlRet = '<div class="action">' +
                            title +
                            '</div>' +
                            '<ul class="actions pull-right">' +
                            '<li><i class="icon-sort-by-size" id="myEventsFilter" style="font-size:40px"></i></li>' +
                            '</ul>'
            return prev + htmlRet + post;
        }
        else {
            return prev + title + post;
        }
    });
});