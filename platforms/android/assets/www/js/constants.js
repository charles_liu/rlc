//constants

constants = {
    "rootUrl" : 'https://www.googleapis.com/calendar/v3/calendars',
    "RLCVerified" : 'eh2dungq2rijrtr0sj32edsn8k@group.calendar.google.com',
    "contactEmail" : 'volunteer@rescuingleftovercuisine.org',
    "calendarEmail" : 'volunteering.rlc@gmail.com',
    "client_id": '131186907793-cejqbdf0jurc7cq6rphir11b58mlmp62.apps.googleusercontent.com',
    "client_secret": 'Hykr1WKM9Yw17ebRoEMO2imn',
    "tentative" : "PENDING",
    "needsAction" : "PENDING",
    "accepted" : "ATTENDING",
    "declined" : "NOT ATTENDING"
};

createEvent = function(calId, accessHeader, data, successText, errorText, goHome, update) {
    var update = update ? true : false;
    var url = update ? constants["rootUrl"] + '/' + calId + '/events/' + data.id + '?sendNotifications=true' : constants["rootUrl"] + '/' + calId + '/events?sendNotifications=true';
    var type = update ? 'PUT' : 'POST';
    var insertOptions = {
        type: type,
        url: url,
        success: function() {
            navigator.notification.alert(successText, function() {}, "Thank You!");
            if (goHome) {
                Backbone.history.navigate("home", {
                    trigger: true
                });
            }
        },
        error: function(xhr) {
            var err = JSON.stringify(xhr);
            data.sequence = data.hasOwnProperty('sequence') ? data.sequence + 1 : 1;
            if (err.indexOf('The requested identifier already exists') != -1 || err.indexOf('Invalid sequence value') != -1) {
                createEvent(calId, accessHeader, data, successText, errorText, goHome, true);
            }
            else
                navigator.notification.alert(errorText, function() {}, "Error");
        },
        data: JSON.stringify(data),
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + accessHeader);
            xhr.setRequestHeader("Content-Type", "application/json");
        }
    };
    Backbone.ajax(insertOptions);
};

rlcIdConvert = function(id) {
    if (id.length <= 1015) {
        return id + 'rlcimport';
    }
    return id.substring(0, 1015) + 'rlcimport';
};

getEventDialog = function(okFunc) {
    return new fries.Dialog({
        selector: '#eventDialog',
        callbackOk: function() {
            okFunc();
            this.hide();
        },
        callbackCancel: function() {
            this.hide();
        }
    });
};

var myScroll = null;

createIScroll = function(page) {
    if (myScroll) {
        myScroll.off('scroll');
        myScroll.off('scrollEnd');
        myScroll.destroy();
    }
    if (typeof page.loadMore === 'function') {
        myScroll = new IScroll('.content', { probeType: 3});
        var loadingMore = false;
        var refreshPage = false;
        var barrier1 = 25;
        var barrier2 = 75;
        myScroll.on('scroll', function() {
            if (myScroll.y <= barrier1) {
                page.partialRender(page.template({
                    title: page.title,
                    showRefresh: false
                }), ['#refreshText']);
                if (refreshPage) {
                    page.collection.fetch({reset: true});
                    refreshPage = false;
                }
            }
            if (myScroll.y > barrier1 && myScroll.y <= barrier2 && !refreshPage) {
                page.partialRender(page.template({
                    title: page.title,
                    showRefresh: true,
                    refreshText: '<i class="icon-chevron-down" style="font-size: 50px;"></i>'
                }), ['#refreshText']);
            }
            else if (myScroll.y > barrier2 && !refreshPage) {
                refreshPage = true;
                page.partialRender(page.template({
                    title: page.title,
                    showRefresh: true,
                    refreshText: '<i class="icon-refresh" style="font-size: 50px;"></i>'
                }), ['#refreshText']);
            }
        });
        myScroll.on('scrollEnd', function(){
            if (myScroll.y - myScroll.maxScrollY < 100 && !loadingMore) {
                loadingMore = true;
                page.loadMore();
                setTimeout(function(){loadingMore = false;}, 100);
            }
        });
    }
    else {
        myScroll = new IScroll('.content');
    }
};

refreshIScroll = function() {
    setTimeout(function(){
        myScroll.refresh();
        if (!myScroll.hasVerticalScroll) {
            var offsetAdd = myScroll.scrollerHeight - myScroll.wrapperHeight + $('#content').css('padding-bottom').replace('px','') + 1;
            $('#content').css('padding-bottom', offsetAdd + 'px');
            setTimeout(function(){myScroll.refresh();}, 100);
        }
    }, 100);
};

getRLCResponse = function(event) {
    if (event.attendees) {
        for (var i = 0 ; i < event.attendees.length ; i++) {
            if (event.attendees[i]["email"].toUpperCase() == constants["contactEmail"].toUpperCase()) {
                return constants[event.attendees[i]["responseStatus"]];
            }
        }
    }
    return "PENDING";
};