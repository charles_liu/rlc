define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/calendar.html",
    "utils/handlebarHelpers"
],
    function ($, _, Backbone, Handlebars, template) {

        var calendarView = Backbone.View.extend({
            events: {
            },

            template: Handlebars.compile(template),

            render: function () {
                //alert(JSON.stringify(this.model.attributes));
                $(this.el).html(this.template({calendar: this.model.toJSON()}));
                return this;
            }
        });

        return calendarView;

    });