define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/structure.html"
],
    function ($, _, Backbone, Handlebars, template) {

        var StructureView = Backbone.View.extend({

            id: "main",

            events: {
                "touchstart .navMyEvents" : "myTouch",
                "touchstart .navAbout" : "aboutTouch",
                "touchstart .navVolunteer" : "volunteerTouch",
                "touchstart #navHome" : "homeTouch",
                "touchstart .navDonate" : "donateTouch",
                "touchstart #navBack" : "backTouch"
            },

            initialize: function () {
                // bind the back event to the goBack function
                //document.getElementById("back").addEventListener("back", this.goBack(), false);
            },

            template: Handlebars.compile(template),

            render: function () {
                // load the template
                this.el.innerHTML = this.template({showBack: false, showTitle: ""});
                // cache a reference to the content element
                this.contentElement = this.$el.find('#content')[0];
                // if(device.platform == "iOS") {
                //   this.contentElement.parentNode.style.marginTop = "20px";
                //   this.contentElement.parentNode.style.height = "calc(100% - 20px)";
                // }
                return this;
            },

            aboutTouch: function (event) {
                Backbone.history.navigate("about", {
                    trigger: true
                });
            },

            volunteerTouch: function (event) {
                Backbone.history.navigate("volunteer", {
                    trigger: true
                });
            },

            homeTouch: function (event) {
                Backbone.history.navigate("home", {
                    trigger: true
                });
            },
            donateTouch: function(event) {
                Backbone.history.navigate("donate", {
                    trigger: true
                });
            },
            myTouch: function(event) {
                Backbone.history.navigate("myEvents", {
                    trigger: true
                });
            },

            backTouch: function(event) {
                Backbone.history.history.back();
            },

            showBack: function(isShow, title) {
                this.partialRender(this.template({showBack: isShow, showTitle: title}), ['#homeDiv']);
            }
        });

        return StructureView;

    });