define(["jquery", "underscore", "backbone", "handlebars", "constants",
    "text!templates/eventList.html",
    "views/eventView"
],
    function ($, _, Backbone, Handlebars, C, template, eventView) {

        var myEventsView = Backbone.View.extend({
            title: "My Events",
            filter: "all",
            collectionIndex: 0,
            events: {
                "click .eventButton": "removeEvent",
                "click #myEventsFilter": "filterEvents"
            },

            initialize: function (options) {
                this.collection.fetch({reset: true});
                this.listenTo(this.collection, "reset remove", this.populateEvents);
                this.filterDialog = null;
            },

            template: Handlebars.compile(template),

            render: function () {
                $(this.el).html(this.template({title: this.title, showRefresh: false}));
                return this;
            },

            populateEvents: function() {
                if (this.$el != null) {
                    var count = 0;
                    this.$('#eventsList').html("");
                    for (var i = 0 ; i < this.collection.length ; i++) {
                        var event = this.collection.at(i);
                        this.collectionIndex = i;
                        if (event.id.indexOf("rlcimport") == event.id.length - 9 && (this.filter == "all" || constants[this.filter] == getRLCResponse(event))) {
                            this.addEvent(event);
                            if (++count == this.collection.count)
                                break;
                        }
                    }
                    if (!count && this.filter == "all") {
                        this.$('#eventsList').html("You have no events! Go to the volunteer page to check some out!");
                    }
                }
                else {
                    //null hack
                }
                refreshIScroll();
            },

            addEvent: function(event) {
                var eView = new eventView({model: event, attributes: {buttonText: "Remove", myEvent: true}});
                this.$('#eventsList').append($(eView.render().el).html());
            },

            removeEvent: function(evt) {
                var self = this;
                var event = self.collection.get(evt.target.id);
                var importOptions = {
                    type: "DELETE",
                    url: constants["rootUrl"] + '/' + self.model.getCalendarID() + '/events/' + evt.target.id,
                    success: function(data) {
                        self.collection.remove(event);
                    },
                    error: function(xhr) {
                        navigator.notification.alert('Could not remove event. Please try again, or you can email us directly at ' + constants["contactEmail"], function() {}, "Error");
                    },
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + self.model.getAuthToken());
                    }
                };
                Backbone.ajax(importOptions);
            },

            loadMore: function(evt) {
                var count = this.collection.count;
                var oldIndex = this.collectionIndex;
                if (oldIndex < this.collection.length) {
                    this.collection.incrementCount();
                    for (var i = oldIndex + 1 ; i < this.collection.length ; i++) {
                        var event = this.collection.at(i);
                        this.collectionIndex = i;
                        if (event.id.indexOf("rlcimport") == event.id.length - 9 && (this.filter == "all" || constants[this.filter] == getRLCResponse(event))) {
                            this.addEvent(event);
                            if (++count == this.collection.count)
                                break;
                        }
                    }
                    refreshIScroll();
                }
            },

            filterEvents: function(evt) {
                var filterOptions = document.querySelectorAll('.filterDialog');
                var self = this;
                var func = this.filterClick;
                if (!this.filterDialog) {
                    this.filterDialog = new fries.Dialog({
                        selector: '#myFilterDialog',
                        callbackCancel: function() {
                            this.hide();
                        }});
                    for (var i = 0 ; i < filterOptions.length ; i++) {
                        filterOptions[i].addEventListener('click',func.bind(self));
                    }
                }
                this.filterDialog.show();
            },

            filterClick: function(evt) {
                this.filter = evt.target.id.replace("resp","");
                this.populateEvents();
            }
        });

        return myEventsView;

    });