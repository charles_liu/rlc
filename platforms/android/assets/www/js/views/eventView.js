define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/event.html",
    "text!templates/myEvent.html",
    "utils/handlebarHelpers"
],
    function ($, _, Backbone, Handlebars, eventTemp, myEventTemp) {

        var eventView = Backbone.View.extend({
            events: {
            },

            render: function () {
                //alert(JSON.stringify(this.model.attributes));
                var template = this.attributes.myEvent ? Handlebars.compile(myEventTemp) : Handlebars.compile(eventTemp);
                $(this.el).html(template({event: this.model.toJSON(), buttonText: this.attributes.buttonText}));
                return this;
            }
        });

        return eventView;

    });