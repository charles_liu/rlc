define(["jquery", "underscore", "backbone", "handlebars",
    "text!templates/home.html"
],
    function ($, _, Backbone, Handlebars, template) {

        var homeView = Backbone.View.extend({

            events: {
            },

            initialize: function (options) {
            },

            template: Handlebars.compile(template),

            render: function () {
                $(this.el).html(this.template({}));
                return this;
            }
        });

        return homeView;

    });