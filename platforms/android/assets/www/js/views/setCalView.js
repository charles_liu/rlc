define(["jquery", "underscore", "backbone", "constants", "handlebars",
    "text!templates/setCal.html",
    "text!templates/newCal.html",
    "views/calendarView"
],
    function ($, _, Backbone, C, Handlebars, template, newCal, calendarView) {

        var setCalView = Backbone.View.extend({
            title: "Please select a calendar. This calendar will be used solely to coordinate RLC events.",
            events: {
                "click .calendarButton" : "selectCal"
            },

            initialize: function (options) {
                this.collection.fetch({reset: true});
                this.listenTo(this.collection, "reset", this.populateCalList);
            },

            loadMore: function() {
                if (this.collection.hasMore) {
                    this.collection.incrementCount();
                }
            },

            template: Handlebars.compile(template),

            render: function () {
                $(this.el).html(this.template({title: this.title, showRefresh:false}));
                return this;
            },

            populateCalList: function() {
                this.$('#calList').html("");
                var newCalTemp = Handlebars.compile(newCal);
                this.$('#calList').append(newCalTemp());
                this.collection.forEach(
                    this.addCal,
                    this
                );
                refreshIScroll();
            },

            addCal: function(calendar) {
                var cView = new calendarView({model: calendar});
                this.$('#calList').append($(cView.render().el).html());
            },

            selectCal: function(event) {
                var id = event.currentTarget.id;
                if (id == "newCal") {
                    var newCalName = $('#newCalName').val();
                    if (newCalName.length > 0) {
                        var accessT = this.model.getAuthToken();
                        var self = this;

                        var calOptions = {
                            type: "POST",
                            url: constants["rootUrl"],
                            success: function(data) {
                                var id = data.id;
                                var calListOptions = {
                                    type: "POST",
                                    url: "https://www.googleapis.com/calendar/v3/users/me/calendarList",
                                    success: function() {
                                        self.model.setCalendarID(id);
                                        Backbone.history.loadUrl(Backbone.history.fragment);
                                    },
                                    error: function() {
                                        navigator.notification.alert('Could not add a calendar. Please try another again.', function() {}, "Error");
                                        var delCalOptions = {
                                            type: "DELETE",
                                            url: constants["rootUrl"] + "/" + id,
                                            beforeSend: function(xhr) {
                                                xhr.setRequestHeader("Authorization", "Bearer " + accessT);
                                            }
                                        };
                                        Backbone.ajax(delCalOptions);
                                    },
                                    data: JSON.stringify({"id": id}),
                                    beforeSend: function(xhr) {
                                        xhr.setRequestHeader("Authorization", "Bearer " + accessT);
                                        xhr.setRequestHeader("Content-Type", "application/json");
                                    }
                                };
                                Backbone.ajax(calListOptions);
                            },
                            error: function(xhr) {
                                navigator.notification.alert('Could not create a calendar. Please try again.', function() {}, "Error");
                            },
                            data: JSON.stringify({"timeZone": "America/New_York","summary": newCalName, "description": "Calendar created from your RLC app." }),
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader("Authorization", "Bearer " + accessT);
                                xhr.setRequestHeader("Content-Type", "application/json");
                            }
                        };
                        Backbone.ajax(calOptions);

                    }
                    else {
                        navigator.notification.alert('You must specify a calendar name.', function() {}, "Error");
                    }
                }
                else {
                    this.model.setCalendarID(id);
                    Backbone.history.loadUrl(Backbone.history.fragment);
                }
            }
        });

        return setCalView;

    });