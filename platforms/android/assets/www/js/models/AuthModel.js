//Model for your login info
define(["jquery", "underscore", "backbone", "constants", "collections/EventCollection"],
    function ($, _, Backbone, C, EventCollection) {

        var AuthModel = Backbone.Model.extend({
            myEvents: new EventCollection(),

            setMyEvents: function() {
                if (this.hasAuthToken() && this.hasCalendarID()) {
                    this.myEvents.configRequest({
                        "request": "GET",
                        "authToken": this.getAuthToken(),
                        "calendarID": this.getCalendarID()
                    });
                }
            },

            checkAuthToken: function() {
                var self = this;
                if (navigator.connection.type == Connection.NONE)
                    navigator.notification.alert("Please check your internet connection then resume the app.", function() {}, "No Connection");
                else if (this.hasAuthToken()) {
                    var tokenJSON = JSON.parse(localStorage.getItem('authToken'));
                    var d = new Date();
                    var tokenDate = new Date();
                    tokenDate.setISO8601(tokenJSON['expDate']);
                    if (tokenDate.getTime() - d.getTime() < 60000) {
                        var refreshOptions = {
                            type: "POST",
                            url: 'https://accounts.google.com/o/oauth2/token',
                            success: function(data) {
                                self.setAuthToken(data);
                                self.setMyEvents();
                            },
                            error: function(xhr) {
                                localStorage.removeItem('authToken');
                            },
                            data: "client_id=" + constants["client_id"] + "&client_secret=" + constants["client_secret"] + "&refresh_token=" + tokenJSON["refresh_token"] + "&grant_type=refresh_token",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            }
                        };
                        Backbone.ajax(refreshOptions);
                        return true;
                    }
                    else
                        this.setMyEvents();
                }
                return false;
            },

            setAuthToken: function(resp) {
                var expDate = new Date();
                expDate.setSeconds(expDate.getSeconds() + resp["expires_in"]);
                resp["expDate"] = expDate;
                if (!resp.hasOwnProperty("refresh_token")) {
                    var currToken  = JSON.parse(localStorage.getItem('authToken'));
                    if (currToken != null && currToken.hasOwnProperty("refresh_token"))
                        resp["refresh_token"] = currToken["refresh_token"];
                }
                localStorage.setItem('authToken' ,JSON.stringify(resp));
                this.setMyEvents();
            },

            hasAuthToken: function() {
                return localStorage.getItem('authToken') != null;
            },

            getAuthToken: function() {
                var tokenJSON = JSON.parse(localStorage.getItem('authToken'));
                return tokenJSON['access_token'];
            } ,

            hasCalendarID: function() {
                return localStorage.getItem('calendarID') != null;
            },

            setCalendarID: function(id) {
                localStorage.setItem('calendarID' ,id);
                this.setMyEvents();
            },

            getCalendarID: function() {
                return localStorage.getItem('calendarID');
            }
        });

        return AuthModel;
    });