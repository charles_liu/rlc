//collection of profile models. set urlpath, userid, logintoken prior to fetching
define(["jquery", "underscore", "backbone", "constants",
    "models/EventModel"
],
    function($, _, Backbone, C,
             EventModel) {

        var EventCollection = Backbone.Collection.extend({
            model: EventModel,
            params: {},
            count: 5,

            urlRoot: constants["rootUrl"],

            url: function() {
                var d = new Date();
                return this.urlRoot + "/" + this.params.calendarID + "/events?timeMin=" + d.toISOString();
            },
            configRequest: function(options) {
                this.params = options;
            },
            sync: function(method, model, options){
                options.timeout = 10000;
                options.type = this.params.request;
                options.headers = {'Authorization': 'Bearer ' + this.params.authToken};
                return Backbone.sync(method, model, options);
            },
            parse: function(response) {
                if (response["items"]) {
                    return response["items"];
                }
                return [];
            },

            incrementCount: function() {
                this.count += 5;
            },

            resetCount: function() {
                this.count = 5;
            }
        });

        return EventCollection;
    });